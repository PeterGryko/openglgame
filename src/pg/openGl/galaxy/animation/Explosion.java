package pg.openGl.galaxy.animation;

import java.util.Random;

import javax.microedition.khronos.opengles.GL10;

import pg.openGl.galaxy.model.Sprite;
import pg.openGl.galaxy.view.GameView;

public class Explosion {

	private float maxSpeed=20;
	private int maxSize=10;
	
	private float x;
	private float y;
	private Random random;
	private GameView view;
	private long lifeTime=1000;
	private long time =0;
	private float r,g,b;
	private Text points;
	
	private  Particle[] particles;
	
	public Explosion(GameView view, int count, float x, float y, long time)
	{
		random = new Random();
		this.view=view;
		this.x=x;
		this.y=y;
		this.time=time;
		
		
		r=(float)Math.random()*0.5f+0.5f;
		g=(float)Math.random()*0.5f+0.5f;;
		b=(float)Math.random()*0.5f+0.5f;
		points = new Text(view,"100",x,y,50,50);
	
		
		particles = new Particle[count];
		for(int i=0;i<count;i++)
		{
			//if(r.nextBoolean())
			
			int partSize=random.nextInt(maxSize-5)+5;
			
			Particle p = new Particle(view,x,y,partSize,partSize,view.getFence());
			p.setMove(randomSign((float)Math.random()*maxSpeed), 
					randomSign((float)Math.random()*maxSpeed));
			particles[i]=p;
		}
		
		
	}
	
	public float randomSign(float number)
	{
		if(random.nextBoolean())
			return number;
		else
			return -number;
		
	}
	
	public void update(long current)
	{
		if(lifeTime<current-time)
			view.removeExplosion(this);
	}
	
	public void draw(GL10 gl)
	{
		
		for(int i=0;i<particles.length;i++)
			particles[i].draw(gl,r,g,b);
		
		points.draw(gl);
		
		
	}
}

