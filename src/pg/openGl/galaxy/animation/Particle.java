package pg.openGl.galaxy.animation;

import javax.microedition.khronos.opengles.GL10;

import pg.openGl.galaxy.model.GameFence;
import pg.openGl.galaxy.model.Sprite;
import pg.openGl.galaxy.view.GameView;

public class Particle extends Sprite {
	
	
	private GameFence fence;

	public Particle(GameView view, float centerX, float centerY, int width,
			int height,GameFence fence) {

		super(view, centerX, centerY, width, height);

		
		this.fence = fence;
		
	}
	
	public void update()
	{super.update();
	
	if(centerX<fence.getFence().left || centerX>fence.getFence().right)
		this.x=-x;
	if(centerY<fence.getFence().top || centerY>fence.getFence().bottom)
		this.y=-y;
		
	
	}

	public void draw(GL10 gl, float r, float g, float b) {
		gl.glLoadIdentity();

		//gl.glColor4f((float) Math.random() * 255, (float) Math.random() * 255,
		//		(float) Math.random() * 255, 0.5f);

	//	gl.glco
		
		gl.glColor4f(r, g, b, 0.0f);
		
		update();
		gl.glTranslatef(moveX, moveY, 0);

		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, bufferedVertex);
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);

	}

}
