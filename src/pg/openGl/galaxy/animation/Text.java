package pg.openGl.galaxy.animation;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import pg.openGl.galaxy.model.Sprite;
import pg.openGl.galaxy.view.GameView;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.opengl.GLUtils;

public class Text extends Sprite {

	private float textureVert[] = { 0.0f, -1.0f, 0.0f, -0.0f, 1.0f, -1.0f, 1.0f,
			-0.0f };
	private FloatBuffer textureBuffer;
	private String text;
	private int[] textures = new int[1];
	private Bitmap bitmap;
	private boolean flag = false;

	public Text(GameView view, String text, float centerX, float centerY,
			int width, int height) {
		super(view, centerX, centerY, width, height);

		ByteBuffer texBuffer = ByteBuffer
				.allocateDirect(textureVert.length * 4);
		texBuffer.order(ByteOrder.nativeOrder());
		textureBuffer = texBuffer.asFloatBuffer();
		textureBuffer.put(textureVert);
		textureBuffer.position(0);

		this.text = text;

		
		
		bitmap = Bitmap.createBitmap(256, 128, Config.ARGB_4444);
		
	
		Canvas c = new Canvas(bitmap);
		Paint p = new Paint();
		//Typeface tf = Typeface.createFromAsset(view.context.getAssets(), "fonts/Mikodacs.otf");
		//p.setTypeface(tf);
		p.setTextSize(128);
		p.setARGB(255, 255, 64, 64);
		p.setAntiAlias(true);
	
		c.drawText(text,0, 128, p);
		
		setMove(0,-1);

	}

	public void loadTexture(GL10 gl) {
	
		if(!flag){
			gl.glGenTextures(1, textures, 0);
			gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
					GL10.GL_NEAREST);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
					GL10.GL_LINEAR);

			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);

			bitmap.recycle();
			flag=true;
		}
		
	}

	public void draw(GL10 gl) {
		gl.glLoadIdentity();
		loadTexture(gl);
		update();
	
	
		gl.glTranslatef(0, moveY, 0);
		
		
		gl.glEnable(GL10.GL_TEXTURE_2D);
		
		
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
	//	gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

		//gl.glColor4f(0.5f, 0.1f, 0.1f, 0.5f);
		gl.glColor4f(1f, 1f, 1f, 1f);
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, bufferedVertex);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);

		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);

	//	gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glDisable(GL10.GL_TEXTURE_2D);
	}

}
