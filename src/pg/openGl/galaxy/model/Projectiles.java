package pg.openGl.galaxy.model;

import javax.microedition.khronos.opengles.GL10;

import pg.openGl.galaxy.view.GameView;

//class describes projectiles
public class Projectiles extends Sprite {

	private GameFence fence;

	public Projectiles(GameView view, float centerX, float centerY, int width,
			int height) {
		super(view, centerX, centerY, width, height);
		this.fence = view.getFence();
	}

	public void draw(GL10 gl) {

		gl.glLoadIdentity();
		if (centerX > fence.getFence().right || centerX < fence.getFence().left
				|| centerY > fence.getFence().bottom
				|| centerY < fence.getFence().top)
			view.removeBullet(this);

		// gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);

		update();

		gl.glTranslatef(moveX, moveY, 0);
		gl.glColor4f(1.0f, 0.0f, 0.5f, 0.5f);

		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, this.bufferedVertex);
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, this.vertices.length / 3);
		// gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

	}

}
