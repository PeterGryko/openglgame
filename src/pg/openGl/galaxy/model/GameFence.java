package pg.openGl.galaxy.model;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import pg.openGl.galaxy.view.GameView;
import android.graphics.RectF;

public class GameFence {

	private float vert[];
	private FloatBuffer bufferedVertex;

	private float x;
	private float y;
	private float width;
	private float height;
	private RectF fence;
	

	private float moveX = 0;
	private float moveY = 0;

	public void setMove(float mx, float my) {
		moveX += mx;
		moveY += my;
		fence.left+=mx;
		fence.right+=mx;
		fence.top+=my;
		fence.bottom+=my;
	}

	public GameFence(GameView view, float x, float y, float width, float height) {

		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.fence = new RectF(x,y,width,height);
		

		float[] tab = { x, y, 0, width, y, 0, width, height, 0, x, height, 0 };

		vert = tab;

		ByteBuffer bytes = ByteBuffer.allocateDirect(vert.length * 4);
		bytes.order(ByteOrder.nativeOrder());

		bufferedVertex = bytes.asFloatBuffer();
		bufferedVertex.put(vert);
		bufferedVertex.position(0);

	}

	public RectF getFence()
	{return fence;}

	public void draw(GL10 gl) {
		gl.glLoadIdentity();

		gl.glTranslatef(moveX, moveY, 0);

		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, bufferedVertex);
		gl.glDrawArrays(GL10.GL_LINE_LOOP, 0, this.vert.length / 3);

	}

}
