package pg.openGl.galaxy.model;

import javax.microedition.khronos.opengles.GL10;

import pg.openGl.galaxy.animation.Explosion;
import pg.openGl.galaxy.animation.Text;
import pg.openGl.galaxy.view.GameView;
import android.util.Log;

import com.example.opengltest.R;

public class GameEnemy extends Sprite{

	private double tan;
	private float moveAngle;
	private float speed;
	private double move_x;
	private double move_y;
	private float r;
	private float g;
	private float b;
	private float enemyRotation =0;
	
	
	private String TAG = "GameEnemy";
	
	public GameEnemy(GameView view, float centerX, float centerY, int width,
			int height) {
		super(view, centerX, centerY, width, height);
		// TODO Auto-generated constructor stub
	
		
		r=(float)Math.random()*0.5f+0.5f;
		g=(float)Math.random()*0.5f+0.5f;
		b=(float)Math.random()*0.5f+0.5f;
	
		
		speed = view.getResources().getDimension(R.dimen.enemy_speed_type1);
	}

	public float estimateRotation(float x, float y) {

		float rotation = (float) Math.toDegrees(Math.atan2((x - this.getCenterX()),
				-(y - this.getCenterY())));

		if (rotation < 0)
			rotation += 360;

		return rotation;

	}
	
	public void setDirection(float x, float y) {

		moveAngle = estimateRotation(x, y);
		// right top
		if (moveAngle > 0 || moveAngle <= 90) {
			tan = Math.sin(Math.toRadians(moveAngle));
			move_x = (speed * tan);

			move_y = Math.sqrt(speed * speed - move_x * move_x);

		}
		// right bottom
		if (moveAngle > 90 && moveAngle <= 180) {

			tan = Math.cos(Math.toRadians(moveAngle - 90));

			move_x = (tan * speed);
			move_y = Math.sqrt(speed * speed - move_x * move_x);
			move_y = -move_y;

		}
		// left bottom
		if (moveAngle > 180 && moveAngle <= 270) {
			tan = Math.sin(Math.toRadians(moveAngle - 180));
			move_x = (tan * speed);
			move_y = Math.sqrt(speed * speed - move_x * move_x);

			move_x = -move_x;
			move_y = -move_y;

		}
		// left top
		if (moveAngle > 270 && moveAngle <= 360) {

			tan = Math.cos(Math.toRadians(moveAngle - 270));

			move_x = (tan * speed);
			move_y = Math.sqrt(speed * speed - move_x * move_x);

			move_x = -move_x;

		}

		
		this.setMove((float)move_x, -(float)move_y);

	}
	
	public void updateAccToFence(float x, float y) {
		//this.setX(this.getX() - x);
		//this.setY(this.getY() - y);
		
		this.setMove(this.x-x, this.y-y);
	}
	
	public boolean checkCollision(Projectiles projectile) {

		float eX = projectile.getCenterX();
		float eY = projectile.getCenterY();

		double distance = Math.sqrt((getCenterX() - eX) * (getCenterX() - eX)
				+ (getCenterY() - eY) * (getCenterY() - eY));

		if (distance < width / 2) {
			Log.d(TAG, "enemy Hitteddd!!! cryticall!!");

			Explosion e = new Explosion(view,50, eX,eY,System.currentTimeMillis());
			
			view.removeEnemy(this);
			view.addExplosion(e);
			return true;
		}
		return false;
	}
	
	public void update()
	{
		super.update();
		enemyRotation+=5;
		if(enemyRotation>360)
			enemyRotation=0;
	}
	
	public void draw(GL10 gl)
	{
		gl.glLoadIdentity();
		
		
		update();
		
		gl.glColor4f(r, g, b, 0.5f);
		
		gl.glTranslatef(centerX, centerY, 0);
		gl.glRotatef(enemyRotation, 0, 0, 1);
		gl.glTranslatef(-centerX, -centerY, 0);
		
		gl.glTranslatef(moveX, moveY, 0);
		
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, bufferedVertex);
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length/3);
		
	}
}
