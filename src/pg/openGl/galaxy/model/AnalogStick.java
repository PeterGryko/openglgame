package pg.openGl.galaxy.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Log;

public class AnalogStick {

	private double moveX;
	private double moveY;
	private float moveAngle = 45;
	private double tan;
	private float x;
	private float y;
	
	private String TAG = "AnalogStick";

	public void setX(float x) {
		this.x = x;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public float getX()
	{return x;}
	
	public float getY()
	{return y;}

	
	// public boolean distanceFC(Bitmap bitmap, float xs, float ys, int i) {
	// if (Math.sqrt((double) (x - xs) * (x - xs) + (y - ys) * (y - ys)) <
	// bitmap
	// .getWidth() / 2 + i)
	// return true;

	// else
	// return false;

	// }

	public boolean leftHalf(float touchX, float screenWidth) {
		if (touchX < screenWidth / 2)
			{
		//	Log.d(TAG, "leftHalf");
			return true;
			}
		else
			return false;

	}
	
	public boolean rightHalf(float touchX, float screenWidth) {
		if (touchX > screenWidth / 2)
			{
		//	Log.d(TAG, "rightHalf");
			return true;
			}
			else
			return false;

	}

	// method sets top part of the stick

	public double getMoveX() {
		return moveX;
	}

	public double getMoveY() {
		return -moveY;
	}

	// method estimates direction of moving/shooting
	public void setDirection(float xs, float ys, float a) {

		moveAngle = estimateRotation(xs, ys, x, y);
		// right top
		if (moveAngle > 0 || moveAngle <= 90) {
			tan = Math.sin(Math.toRadians(moveAngle));
			moveX = (a * tan);

			moveY = Math.sqrt(a * a - moveX * moveX);

		}
		// right bottom
		if (moveAngle > 90 && moveAngle <= 180) {

			tan = Math.cos(Math.toRadians(moveAngle - 90));

			moveX = (tan * a);
			moveY = Math.sqrt(a * a - moveX * moveX);
			moveY = -moveY;

		}
		// left bottom
		if (moveAngle > 180 && moveAngle <= 270) {
			tan = Math.sin(Math.toRadians(moveAngle - 180));
			moveX = (tan * a);
			moveY = Math.sqrt(a * a - moveX * moveX);

			moveX = -moveX;
			moveY = -moveY;

		}
		// left top
		if (moveAngle > 270 && moveAngle <= 360) {

			tan = Math.cos(Math.toRadians(moveAngle - 270));

			moveX = (tan * a);
			moveY = Math.sqrt(a * a - moveX * moveX);

			moveX = -moveX;

			
			
		}
	//	Log.d(TAG, Double.toString(moveX)+"  "+Double.toString(moveY));
	}

	// angle between two points
	public float estimateRotation(float x, float y, float xs, float ys) {

		// xTop = (int) x;
		// yTop = (int) y;

		float rotation = (float) Math.toDegrees(Math.atan2(x - xs, -(y - ys)));

		if (rotation < 0)
			rotation += 360;

		//Log.d(TAG, Float.toString(rotation));
		
		return rotation;

	}

}
