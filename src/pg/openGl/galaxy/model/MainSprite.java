package pg.openGl.galaxy.model;

import javax.microedition.khronos.opengles.GL10;

import pg.openGl.galaxy.view.GameView;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

import com.example.opengltest.R;

public class MainSprite extends Sprite {

	private String TAG = "MainSprite";
	private GameFence fence;
	private Background background;

	private float angle;

	public void setRotation(float angle) {
		this.angle = angle;
	}

	public MainSprite(GameView view, float centerX, float centerY, int width,
			int height) {

		super(view, centerX, centerY, width, height);
		fence = view.getFence();
		background = view.getGameBackground();

	}

	public void loadGLTexture(GL10 gl, Context context) {

		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
				R.drawable.enemy);

		// generate one texture pointer

		// gl.glGenTextures(1, textures, 0);

		// ...and bind it to our array

		// gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

		// create nearest filtered texture

		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
				GL10.GL_NEAREST);

		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
				GL10.GL_LINEAR);

		// Use Android GLUtils to specify a two-dimensional texture image from
		// our bitmap

		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);

		// Clean up

		bitmap.recycle();

	}

	public void update() {
		super.update();
		fence.setMove(-x, -y);
		background.setMove(-x, -y);
	}

	public void draw(GL10 gl) {

		// Point to our buffers

		// gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);

		// gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

		// gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

		// gl.glRotatef(angle, centerX, centerY, 0);
		// angle-=5;

		gl.glLoadIdentity();

		// Log.d(TAG, Float.toString(x) + "  "+Float.toString(y));

		if (centerX > fence.getFence().right)
			x = -1;
		if (centerY > fence.getFence().bottom)
			y = -1;
		if (centerX < fence.getFence().left)
			x = 1;
		if (centerY < fence.getFence().top)
			y = 1;

		// Log.d(TAG, Integer.toString(view.screenWidth) + "  "+
		// Float.toString(centerX));

		update();

		gl.glTranslatef(centerX, centerY, 0);
		gl.glRotatef(angle, 0, 0, 1);
		gl.glTranslatef(-centerX, -centerY, 0);

		gl.glTranslatef(moveX, moveY, 0);

		// Log.d("sfsd", Float.toString(moveX)+"  "+Float.toString(moveY));

		gl.glColor4f(0.0f, 1.0f, 0.0f, 0.5f);

		// if(angle==0)
		// angle=360;
		// Set the face rotation

		// gl.glFrontFace(GL10.GL_CW);

		// Point to our vertex buffer

		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, this.bufferedVertex);

		// gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);

		// Draw the vertices as triangle strip

		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);
		// gl.glDrawArrays(GL10.gl_line, arg1, arg2);.

		// Disable the client state before leaving

		// gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

		// gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

	}

}
