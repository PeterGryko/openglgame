package pg.openGl.galaxy.model;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import pg.openGl.galaxy.view.GameView;

public class Sprite {

	public float centerX; // used to track sprite position
	public float centerY;
	public float moveX; // used to sprite translation
	public float moveY;
	public float width;
	public float height;
	public float vertices[];
	public FloatBuffer bufferedVertex;
	public GameView view;

	public float x; // used to increas/decreas sprite position
	public float y;

	public Sprite(GameView view, float centerX, float centerY, int width,
			int height) {
		this.view = view;
		this.centerX = centerX;
		this.centerY = centerY;
		this.width = width;
		this.height = height;
		float tab[] = { (centerX - width / 2), (centerY - height / 2), 0, // bottom
																			// left
				(centerX - width / 2), (centerY + height / 2), 0, // top left
				(centerX + width / 2), (centerY - height / 2), 0, // bottom
																	// right
				(centerX + width / 2), (centerY + height / 2), 0 };// top right
		vertices = tab;

		ByteBuffer bytes = ByteBuffer.allocateDirect(vertices.length * 4);
		bytes.order(ByteOrder.nativeOrder());

		bufferedVertex = bytes.asFloatBuffer();
		bufferedVertex.put(vertices);
		bufferedVertex.position(0);
	}

	public void setMove(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public float getMoveX() {
		return x;
	}

	public float getMoveY() {
		return y;
	}

	public void update() {
		moveX += x;
		moveY += y;
		centerX += x;
		centerY += y;
	}

	public float getCenterX() {
		return centerX;
	}

	public float getCenterY() {
		return centerY;
	}

}
