package pg.openGl.galaxy.spawn;

import java.util.Random;

import pg.openGl.galaxy.animation.Text;
import pg.openGl.galaxy.model.GameEnemy;
import pg.openGl.galaxy.view.GameView;
import android.graphics.RectF;

import com.example.opengltest.R;

public class SpawnEnemy {

	private GameView view;
	private int delay;
	// private int type;
	private long lastTime = 0;
	private String TAG = "SpawnEnemy";
	private int maxSize;
	private int minSize;
	private int size;
	private Random r = new Random();
	private RectF f;
	

	//
	public SpawnEnemy(GameView view) {
		
		this.view = view;
		maxSize = view.screenWidth/20;
		minSize = maxSize/3;
		delay = view.getResources().getInteger(R.integer.enemy_delay_type1);
        f=view.getFence().getFence();
	}

	// starts thead
	public void spawn(long time) {

		// create new bullet and estimate flight direction

		float x = 0;
		float y = 0;

		if (time > lastTime + delay) {
			for (int i = 0; i < 3; i++) {
				int index = r.nextInt(3) + 1;
				if (index == 1) {
					x =f.left+ 20;
					y = (float) Math.random() * f.height()+f.top;
				} else if (index == 2) {
					x = (float) Math.random() * f.width()+f.top;
					y = f.top+20;
				} else if (index == 3) {
					x = (float) Math.random() * f.width()+f.top;
					y = f.top+f.height() - 20;
				}
				if (index == 4) {
					x = f.left+f.width() - 20;
					y = (float) Math.random() * f.height()+f.top;
				}

				lastTime = time;
				size = r.nextInt(maxSize-minSize)+maxSize;
				GameEnemy e = new GameEnemy(view, x, y, size, size);
				view.addEnemy(e);
			}
		}

	}

}
