package pg.openGl.galaxy.spawn;

import pg.openGl.galaxy.model.Projectiles;
import pg.openGl.galaxy.view.GameView;

public class SpawnProjectile {

	private GameView view;
	private int delay;

	private long lastTime = 0;
	private String TAG = "SpawnProjectiles";

	//
	public SpawnProjectile(GameView view) {

		this.view = view;
		delay = 100;

	}

	public void spawn(float x, float y, float xm, float ym, long time) {

		if (time > lastTime + delay) {

			lastTime = time;
			Projectiles p = new Projectiles(view, x, y, 10, 10);
			p.setMove(xm, ym);
			view.addBullet(p);

		}

	}
}
