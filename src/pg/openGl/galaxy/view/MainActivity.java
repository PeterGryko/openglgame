package pg.openGl.galaxy.view;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {

	private String TAG = "MainActivity";
	private GameView game;
	public GLSurfaceView glSurfaceView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
				WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
		// making it full screen

		game = new GameView(this);
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

	//	game.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		
		
		setContentView(game);

	}

//	public void onWindowFocusChanged(boolean flag)
//	{
	//	Log.d(TAG, Boolean.toString(game.isHardwareAccelerated()));
//	}
	
	public void onResume() {

		super.onResume();
		game.onResume();
	}

	public void onPause() {

		super.onPause();
		game.onPause();

	}

}
