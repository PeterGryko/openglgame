package pg.openGl.galaxy.view;

import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import pg.openGl.galaxy.animation.Explosion;
import pg.openGl.galaxy.model.AnalogStick;
import pg.openGl.galaxy.model.Background;
import pg.openGl.galaxy.model.GameEnemy;
import pg.openGl.galaxy.model.GameFence;
import pg.openGl.galaxy.model.MainSprite;
import pg.openGl.galaxy.model.Projectiles;
import pg.openGl.galaxy.spawn.SpawnEnemy;
import pg.openGl.galaxy.spawn.SpawnProjectile;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;

import com.example.opengltest.R;

public class GameView extends GLSurfaceView implements GLSurfaceView.Renderer {

	private String TAG = "GameView";

	private MainSprite mainSprite;
	private Background background;
	private GameFence fence;
	public Context context;

	private AnalogStick leftStick;
	private AnalogStick rightStick;
	//private Text points;
	private int lIndex;
	private int rIndex;

	
	public int screenWidth;
	public int screenHeight;

	private float spriteSpeed = this.getResources().getDimension(R.dimen.speed);
	private float bulletSpeed = this.getResources().getDimension(
			R.dimen.bullet_speed);

	private boolean isShooting = false;

	private SpawnProjectile spawnProjectiles;
	private SpawnEnemy spawnEnemies;

	private ArrayList<Projectiles> projectiles = new ArrayList<Projectiles>();
	private ArrayList<GameEnemy> enemies = new ArrayList<GameEnemy>();
	private ArrayList<Explosion> explosions = new ArrayList<Explosion>();

	public GameView(Context context) {
		super(context);
		this.setRenderer(this);
		this.context = context;

	
		screenWidth = this.getResources().getDisplayMetrics().widthPixels;
		screenHeight = this.getResources().getDisplayMetrics().heightPixels;

		background = new Background(this);
		
		fence = new GameFence(this, -screenWidth / 4, -screenHeight / 4,
				screenWidth + screenWidth / 4, screenHeight + screenHeight / 4);

		mainSprite = new MainSprite(this, screenWidth / 2, screenHeight / 2,
				screenWidth/20, screenWidth/20);
	//	points = new Text(this, "sfsdf", 100, 100, 100, 50);
		leftStick = new AnalogStick();
		rightStick = new AnalogStick();
		spawnProjectiles = new SpawnProjectile(this);
		spawnEnemies = new SpawnEnemy(this);
		
		
		
	}

	
	
	public GameFence getFence() {
		return fence;
	}
	
	public Background getGameBackground()
	{return background;}

	public void addBullet(Projectiles p) {
		projectiles.add(p);
	}

	public void removeBullet(Projectiles p) {
		projectiles.remove(p);
	}

	public void addEnemy(GameEnemy enemy) {
		enemies.add(enemy);
	}

	public void removeEnemy(GameEnemy enemy) {
		enemies.remove(enemy);
	}

	public void addExplosion(Explosion e) {
		explosions.add(e);
	}

	public void removeExplosion(Explosion e) {
		explosions.remove(e);
		// Log.d(TAG, Integer.toString(explosions.size()));
	}

	private void update() {
		if (isShooting)
			spawnProjectiles.spawn(mainSprite.getCenterX(),
					mainSprite.getCenterY(), (float) rightStick.getMoveX(),
					(float) rightStick.getMoveY(), System.currentTimeMillis());

		spawnEnemies.spawn(System.currentTimeMillis());

		for (int i = 0; i < projectiles.size(); i++) {
			for (int j = 0; j < enemies.size(); j++) {
				enemies.get(j).checkCollision(projectiles.get(i));
			}
		}
	}

	@Override
	public void onDrawFrame(GL10 gl) {
		// TODO Auto-generated method stub
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		gl.glLoadIdentity();

		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);

		
		background.draw(gl);
		
		mainSprite.draw(gl);

		fence.draw(gl);

		for (int i = 0; i < enemies.size(); i++) {
			enemies.get(i).setDirection(mainSprite.getCenterX(),
					mainSprite.getCenterY());
			enemies.get(i).updateAccToFence(mainSprite.getMoveX(),
					mainSprite.getMoveY());
			enemies.get(i).draw(gl);

		}

		for (int i = 0; i < projectiles.size(); i++) {

			projectiles.get(i).draw(gl);
		}

		for (int i = 0; i < explosions.size(); i++) {
			explosions.get(i).draw(gl);
			explosions.get(i).update(System.currentTimeMillis());
		}

	///	points.draw(gl);
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

		update();
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {

		if (height == 0)
			height = 1;

		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();

		gl.glOrthof(0, width, height, 0, -1, 1);
		gl.glViewport(0, 0, width, height);

		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
	//	points.loadTexture(gl);
	//	gl.glEnable(GL10.GL_TEXTURE_2D);
		background.loadTexture(gl);
		
	
	}

	// method work when we release finger from analog stick
	protected void releaseFinger(MotionEvent event) {
		// if release right stick top and zero right finger index

		if (event.getPointerId(event.getActionIndex()) + 1 == rIndex) {

			rIndex = 0;

			isShooting = false;

		}

		// if release left stick ship stops, left stick centers and index sets
		// to 0
		if (event.getPointerId(event.getActionIndex()) + 1 == lIndex) {

			mainSprite.setMove(0, 0);
			lIndex = 0;

		}
	}

	public void setStick(MotionEvent event) {

		int i = event.getPointerCount() - 1;

		if (leftStick.leftHalf(event.getX(i), screenWidth) && lIndex == 0) {
			leftStick.setX(event.getX(i));
			leftStick.setY(event.getY(i));
			Log.d(TAG, "left");

		} else if (rightStick.rightHalf(event.getX(i), screenWidth)
				&& rIndex == 0) {
			rightStick.setX(event.getX(i));
			rightStick.setY(event.getY(i));
			Log.d(TAG, "right");
		}

	}

	// when touch the screen
	protected void putFinger(MotionEvent event) {
		// in loop we check each finger on the screen
		for (int i = 0; i < event.getPointerCount(); ++i) {
			if (event.getPointerId(i) >= 0) {
				// check if left analog is being touch

				if (leftStick.leftHalf(event.getX(i), screenWidth)) {

					// if set lIndex
					lIndex = event.getPointerId(i) + 1;

					// estimate angle beetween analog center and finger
					leftStick.setDirection(event.getX(i), event.getY(i),
							spriteSpeed);
					// based on angle, we set current ship direction

					mainSprite.setMove((float) leftStick.getMoveX(),
							(float) leftStick.getMoveY());

				}
				// if on right stick
				if (rightStick.rightHalf(event.getX(i), screenWidth)) {
					isShooting = true;
					// set right finger id
					rIndex = event.getPointerId(i) + 1;
					// estimate ship rotation based on angle between finger and
					// stick center

					mainSprite.setRotation(rightStick.estimateRotation(
							event.getX(i), event.getY(i), rightStick.getX(),
							rightStick.getY()));

					// based on stick angle, we estimate shooting direction
					rightStick.setDirection(event.getX(i), event.getY(i),
							bulletSpeed);

				}

			}

		}
	}

	// touch events
	public boolean onTouchEvent(MotionEvent event) {

		switch (event.getActionMasked()) {

		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_POINTER_DOWN: {
			setStick(event);

		}
			break;
		case MotionEvent.ACTION_MOVE: {

			putFinger(event);

		}
			break;

		case MotionEvent.ACTION_POINTER_UP: {

			releaseFinger(event);
		}
			break;

		case MotionEvent.ACTION_UP: {
			releaseFinger(event);
		}
			break;

		}

		return true;

	}

}
